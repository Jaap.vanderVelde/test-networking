import os
import psutil
import socket
import dns.resolver


def main():
    with open('curl_errors.txt', 'r') as f:
        curl_errors = ['No error'] + [line.strip() for line in f.readlines()]

    resolver = dns.resolver.Resolver()
    for addr, interfaces in psutil.net_if_addrs().items():
        for iface in interfaces:
            if iface.family == socket.AddressFamily.AF_INET:
                if errorlevel := os.system(
                        f'curl --interface {iface.address} http://www.msftncsi.com/ncsi.txt > nul 2>&1'):
                    print(f'{iface.address}, error {errorlevel}: {curl_errors[errorlevel]}')
                else:
                    print(f'{iface.address}, success')

                try:
                    resolver.resolve(
                        'dns.msftncsi.com',
                        source=iface.address,
                        lifetime=10
                    )
                    print(f'{iface.address}, DNS resolved')
                except dns.resolver.NoNameservers:
                    print(f'{iface.address}, DNS no resolution')
                except dns.exception.Timeout:
                    print(f'{iface.address}, DNS timed out')


if __name__ == '__main__':
    main()
